﻿using Nancy;
using Nancy.Diagnostics;

namespace IzendaHours.API.Bootstrappers
{
    public class HoursBootstrapper : DefaultNancyBootstrapper
    {
        /// <summary>
        /// Activate NancyFx Diagnostic Tools
        /// </summary>
        protected override DiagnosticsConfiguration DiagnosticsConfiguration
        {
            get { return new DiagnosticsConfiguration { Password = @"testpassword" }; }
        }
    }
}