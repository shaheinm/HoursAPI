﻿using Nancy;
using Nancy.Responses;
using IzendaHours.API.Models;

namespace IzendaHours.API.Exceptions
{
    public class ErrorBuilder
    {
        /// <summary>
        /// Form error messages to NancyFx standard
        /// </summary>
        /// <param name="url">Error URL</param>
        /// <param name="verb">HTTP Verb Used</param>
        /// <param name="code">Status Code at URL</param>
        /// <param name="errorMessage">Default error message</param>
        /// <returns>Error response in JSON format</returns>
        public static Response ErrorResponse(string url, string verb, HttpStatusCode code, string errorMessage)
        {
            ErrorBody e = new ErrorBody
            {
                Url = url,
                Operation = verb,
                Message = errorMessage
            };
            // Build and return an object that the Nancy server knows about.
            Response response = new JsonResponse<ErrorBody>(e, new DefaultJsonSerializer());
            response.StatusCode = code;
            return response;
        }

    }
}