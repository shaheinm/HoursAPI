﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IzendaHours.API.Models
{
    /// <summary>
    /// Useful info to return in an error
    /// </summary>
    public class ErrorBody
    {
        public string Url { get; set; }
        public string Operation { get; set; }
        public string Message { get; set; }
    }
}