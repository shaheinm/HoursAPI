﻿using Nancy;
using System;
using IzendaHours.API.Exceptions;

namespace IzendaHours.API.Modules
{
    public class ErrorModule : NancyModule
    {
        /// <summary>
        /// Handle exceptions in a manner that assists in debugging
        /// </summary>
        /// <param name="e">Error that occurred</param>
        /// <param name="operation"></param>
        /// <returns>Complete Error Message</returns>
        public Response HandleException(Exception e, string operation)
        {
            // Operation attempted
            string errorContext = string.Format("{1}:{2}: {3} Exception caught in: {0}", operation, DateTime.UtcNow.ToShortDateString(), DateTime.UtcNow.ToShortTimeString(), e.GetType());
            // Detailed output to server console
            Console.WriteLine("----------------------\n{0}\n{1}\n--------------------", errorContext, e.Message);
            if (e.InnerException != null)
                Console.WriteLine("{0}\n--------------------", e.InnerException.Message);
            // Response output
            return ErrorBuilder.ErrorResponse(this.Request.Url.ToString(), "GET", HttpStatusCode.InternalServerError, "Operational difficulties");
        }
        /// <summary>
        /// Show raw error data in readable format to assist in debugging
        /// </summary>
        /// <returns>Error data</returns>
        public string GetBodyRaw()
        {
            // For error writing purposes
            byte[] b = new byte[this.Request.Body.Length];
            this.Request.Body.Read(b, 0, Convert.ToInt32(this.Request.Body.Length));
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            string bodyData = encoding.GetString(b);
            return bodyData;
        }
    }
}