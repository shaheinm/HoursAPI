using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses;
using System;
using System.Collections.Generic;
using IzendaHours.Repository.ProjectsRepository;
using IzendaHours.Framework.Models;
using IzendaHours.API.Exceptions;

namespace IzendaHours.API.Modules
{
    public class ProjectModule : NancyModule
    {
        /// <summary>
        /// Project route tree
        /// </summary>
        public ProjectModule() : base("/api/project")
        {
            Get["/"] = parameter => GetAllProjects();

            Get["/{id}"] = parameter => GetByProjectId(parameter.id);

            Post["/"] = parameter => this.AddProject();

            Put["/{id}"] = parameter => this.UpdateProject(parameter.id);

            Delete["/{id}"] = parameter => this.DeleteProject(parameter.id);
        }
        /// <summary>
        /// Get all projects as a List
        /// </summary>
        /// <returns>Project List</returns>
        private object GetAllProjects()
        {
            try
            {
                ProjectsRepository ctx = new ProjectsRepository();

                IList<Projects> res = ctx.GetProjects();

                return res;
            }
            catch (Exception e)
            {
                return em.HandleException(e, string.Format("ProjectModule.GetAll()"));
            }
        }
        /// <summary>
        /// Get an individual project
        /// </summary>
        /// <param name="id">Project ID</param>
        /// <returns>Individual project JSON</returns>
        private object GetByProjectId(int id)
        {
            try
            {
                ProjectsRepository ctx = new ProjectsRepository();
                Projects res = ctx.GetByProjectId(id);
                if (res == null)
                    return HttpStatusCode.NotFound;
                else
                    return res;
            }
            catch
            {
                return HttpStatusCode.BadRequest;
            }
        }
        /// <summary>
        /// Add a new project to the database
        /// </summary>
        /// <returns>Success or Failure Status Code</returns>
        Response AddProject()
        {
            Projects project = null;
            try
            {
                project = this.Bind<Projects>();

                ProjectsRepository ctx = new ProjectsRepository();
                ctx.AddProject(project);

                Response response = new JsonResponse<Projects>(project, new DefaultJsonSerializer());
                response.StatusCode = HttpStatusCode.Created;

                string uri = this.Request.Url.SiteBase + this.Request.Path + "/" + project.ProjectId.ToString();
                response.Headers["Location"] = uri;
                response.Headers["Content-Type"] = "application/json";

                return response;
            }
            catch (Exception e)
            {
                string rawBody = em.GetBodyRaw();
                Console.WriteLine(rawBody);
                string operation = string.Format("ProjectModule.AddProject({0})", (project == null) ? "No Model Data" : project.Project);
                return em.HandleException(e, operation);
            }
        }
        /// <summary>
        /// Modify an existing project
        /// </summary>
        /// <param name="id">Project ID</param>
        /// <returns>Success or Failure Status Code</returns>
        Response UpdateProject(int id)
        {
            Projects project = null;
            try
            {
                project = this.Bind<Projects>();

                ProjectsRepository ctx = new ProjectsRepository();

                project.ProjectId = id;
                Projects res = ctx.GetByProjectId(id);
                if (res == null)
                {
                    return 404;
                }

                ctx.UpdateProject(project);
                return 204;
            }
            catch (Exception e)
            {

                string operation = string.Format("ProjectModule.AddProject({0})", (project == null) ? "No Model Data" : project.Project);
                return em.HandleException(e, operation);
            }
        }
        /// <summary>
        /// Remove an existing project
        /// </summary>
        /// <param name="id">Project ID</param>
        /// <returns>Success or Failure Status Code</returns>
        Response DeleteProject(int id)
        {
            try
            {
                ProjectsRepository ctx = new ProjectsRepository();
                Projects res = ctx.GetByProjectId(id);

                if (res == null)
                    return ErrorBuilder.ErrorResponse(this.Request.Url.ToString(), "DELETE", HttpStatusCode.NotFound, string.Format("A project with ProjectId = {0} does not exist", id));

                Projects ci = new Projects();
                ci.ProjectId = id;
                ctx.DeleteProject(ci);

                return 204;
            }
            catch (Exception e)
            {
                return em.HandleException(e, string.Format("\nProjectController.Delete({0})", id));
            }
        }
        /// <summary>
        /// Inject error handling methods
        /// </summary>
        private ErrorModule em = new ErrorModule();
    }
}