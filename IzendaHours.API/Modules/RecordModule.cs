using System.Collections.Generic;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses;
using System;
using IzendaHours.Repository.RecordsRepository;
using IzendaHours.Framework.Models;
using IzendaHours.API.Exceptions;

namespace IzendaHours.API.Modules
{
    public class RecordModule : NancyModule
    {
        /// <summary>
        /// Records route tree
        /// </summary>
        public RecordModule() : base("/api/record")
        {
            Get["/"] = parameter => GetAll();

            Get["/{id}"] = parameter => GetById(parameter.id);

            Post["/"] = parameter => this.AddRecord();

            Put["/{id}"] = parameter => this.UpdateRecord(parameter.id);

            Delete["/{id}"] = parameter => this.DeleteRecord(parameter.id);
        }
        /// <summary>
        /// Get all records in a List
        /// </summary>
        /// <returns>Records as a List</returns>
        private object GetAll()
        {
            try
            {
                RecordsRepository ctx = new RecordsRepository();

                string currentUser = "";
                string loggedInUser = currentUser;
                if (!string.IsNullOrEmpty(currentUser))
                    loggedInUser = "'" + currentUser + "'";

                IList<Records> res = ctx.Get(loggedInUser);

                return res;
            }
            catch (Exception e)
            {
                return em.HandleException(e, string.Format("RecordModule.GetAll()"));
            }
        }
        /// <summary>
        /// Get a single Record
        /// </summary>
        /// <param name="id">Record ID</param>
        /// <returns>Single record</returns>
        private object GetById(int id)
        {
            try
            {
                RecordsRepository ctx = new RecordsRepository();
                Records res = ctx.GetById(id);
                if (res == null)
                    return HttpStatusCode.NotFound;
                else
                    return res;
            }
            catch
            {
                return HttpStatusCode.BadRequest;
            }
        }
        /// <summary>
        /// Add a record to the database
        /// </summary>
        /// <returns>Success or Failure Status Code</returns>
        Response AddRecord()
        {
            Records record = null;
            try
            {
                record = this.Bind<Records>();

                RecordsRepository ctx = new RecordsRepository();
                ctx.Add(record);

                Response response = new JsonResponse<Records>(record, new DefaultJsonSerializer());
                response.StatusCode = HttpStatusCode.Created;

                string uri = this.Request.Url.SiteBase + this.Request.Path + "/" + record.EntryId.ToString();
                response.Headers["Location"] = uri;
                response.Headers["Content-Type"] = "application/json";

                return response;
            }
            catch (Exception e)
            {
                string rawBody = em.GetBodyRaw();
                Console.WriteLine(rawBody);
                string operation = string.Format("RecordModule.AddRecord({0})", (record == null) ? "No Model Data" : record.EmployeeId);
                return em.HandleException(e, operation);
            }
        }
        /// <summary>
        /// Update an individual record
        /// </summary>
        /// <param name="id">Record ID</param>
        /// <returns>Success or Failure Status Code</returns>
        Response UpdateRecord(int id)
        {
            Records record = null;
            try
            {
                record = this.Bind<Records>();

                RecordsRepository ctx = new RecordsRepository();

                record.EntryId = id;
                Records res = ctx.GetById(id);
                if(res == null)
                {
                    return 404;
                }

                ctx.Update(record);
                return 204;
            }
            catch (Exception e)
            {
                string operation = string.Format("RecordModule.AddRecord({0})", (record == null) ? "No Model Data" : record.EmployeeId);
                return em.HandleException(e, operation);
            }
        }
        /// <summary>
        /// Delete a single record
        /// </summary>
        /// <param name="id">Record ID</param>
        /// <returns>Success or Failure Status Code</returns>
        Response DeleteRecord(int id)
        {
            try
            {
                RecordsRepository ctx = new RecordsRepository();
                Records res = ctx.GetById(id);

                if (res == null)
                    return ErrorBuilder.ErrorResponse(this.Request.Url.ToString(), "DELETE", HttpStatusCode.NotFound, string.Format("A record with EntryId = {0} does not exist", id));

                Records ci = new Records();
                ci.EntryId = id;
                ctx.Delete(ci);

                return 204;
            }
            catch (Exception e)
            {
                return em.HandleException(e, string.Format("\nRecordController.Delete({0})", id));
            }
        }
        /// <summary>
        /// Inject error handling methods
        /// </summary>
        private ErrorModule em = new ErrorModule();
    }
}