using Nancy;
using System.Collections.Generic;
using IzendaHours.Framework.Models;
using IzendaHours.Repository.TasksRepository;

namespace IzendaHours.API.Modules
{
    public class TaskModule : NancyModule
    {
        /// <summary>
        /// Tasks route tree
        /// </summary>
        public TaskModule() : base("/api/task")
        {
            Get["/"] = _ => GetAllTasks();
        }
        /// <summary>
        /// Get a List of Tasks
        /// </summary>
        /// <returns>All Tasks as a List</returns>
        private object GetAllTasks()
        {
            try
            {
                TasksRepository ctx = new TasksRepository();
                IList<Tasks> res = ctx.GetTasks();

                return res;
            }
            catch
            {
                return HttpStatusCode.BadRequest;
            }
        }
    }
}