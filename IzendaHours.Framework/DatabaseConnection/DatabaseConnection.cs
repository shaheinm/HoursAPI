﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace IzendaHours.Framework.DatabaseConnection
{
    /// <summary>
    /// Base class for database connections
    /// </summary>
    public class DatabaseConnection
    {
        /// <summary>
        /// Establish a connection to the database
        /// </summary>
        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            }
        }
    }
}
