﻿using System;
using Dapper.Contrib.Extensions;

namespace IzendaHours.Framework.Models
{
    [Table("Records")]
    public class Records
    {
        [ExplicitKey]
        public int EntryId { get; set; }
        public string EmployeeId { get; set; }
        public int TaskId { get; set; }
        public string CaseNo { get; set; }
        public int ProjectId { get; set; }
        public decimal? Hours { get; set; }
        public string WikiLink { get; set; }
        public string Notes { get; set; }
        public DateTime RecordDate { get; set; }
        public string Project { get; set; }
        public string Task { get; set; }
    }
}
