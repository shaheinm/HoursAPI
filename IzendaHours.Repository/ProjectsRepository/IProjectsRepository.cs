﻿using System.Collections.Generic;
using IzendaHours.Framework.Models;

namespace IzendaHours.Repository.ProjectsRepository
{
    internal interface IProjectsRepository
    {
        IList<Projects> GetProjects();

        Projects GetByProjectId(int id);

        bool AddProject(Projects project);

        bool UpdateProject(Projects project);

        bool DeleteProject(Projects project);
    }
}
