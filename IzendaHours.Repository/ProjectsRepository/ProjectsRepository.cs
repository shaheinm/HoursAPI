﻿using System.Collections.Generic;
using IzendaHours.Framework.Models;
using Dapper;
using System.Data;
using DapperExtensions;
using IzendaHours.Framework.DatabaseConnection;

namespace IzendaHours.Repository.ProjectsRepository
{
    public class ProjectsRepository : DatabaseConnection, IProjectsRepository
    {
        /// <summary>
        /// Select all projects as a List
        /// </summary>
        /// <returns>A list of all projects</returns>
        public IList<Projects> GetProjects()
        {
            IList<Projects> project = null;

            using (IDbConnection conn = Connection)
            {
                conn.Open();
                project = conn.Query<Projects>("SELECT * FROM Projects ORDER BY ProjectId").AsList();
            }

            return project;
        }
        /// <summary>
        /// Select an individual project
        /// </summary>
        /// <param name="id">Project ID</param>
        /// <returns>A single project</returns>
        public Projects GetByProjectId(int id)
        {
            Projects project = null;

            using (IDbConnection conn = Connection)
            {
                conn.Open();
                project = conn.Get<Projects>(id);
            }

            return project;
        }
        /// <summary>
        /// Add a new project to the Database
        /// </summary>
        /// <param name="project">Project object</param>
        /// <returns>Whether or not rows were updated</returns>
        public bool AddProject(Projects project)
        {
            int rowsAffected = 0;
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                rowsAffected = conn.Execute(@"INSERT INTO Projects (Project) VALUES (@Project)",
                                 new { Project = project.Project });
            }

            if (rowsAffected > 0)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Update a project in the database
        /// </summary>
        /// <param name="project">Project Object</param>
        /// <returns>Whether or not rows were updated</returns>
        public bool UpdateProject(Projects project)
        {
            bool rowsAffected = false;

            using (IDbConnection conn = Connection)
            {
                conn.Open();
                rowsAffected = conn.Update(project);
            }

            return rowsAffected;
        }
        /// <summary>
        /// Delete a project from the database
        /// </summary>
        /// <param name="project">Project object</param>
        /// <returns>Whether or not rows were updated</returns>
        public bool DeleteProject(Projects project)
        {
            bool rowsAffected = false;

            using (IDbConnection conn = Connection)
            {
                conn.Open();
                rowsAffected = conn.Delete(project);
            }

            return rowsAffected;
        }
    }
}
