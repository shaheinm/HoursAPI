﻿using System.Collections.Generic;
using IzendaHours.Framework.Models;

namespace IzendaHours.Repository.RecordsRepository
{
    internal interface IRecordsRepository
    {
        IList<Records> Get(string user);

        Records GetById(int id);

        bool Add(Records record);

        bool Update(Records record);

        bool Delete(Records record);

    }
}
