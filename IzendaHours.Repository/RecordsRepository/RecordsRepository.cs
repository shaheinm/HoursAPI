﻿using System.Collections.Generic;
using IzendaHours.Framework.Models;
using Dapper;
using System.Data;
using System.Linq;
using Dapper.Contrib.Extensions;
using IzendaHours.Framework.DatabaseConnection;

namespace IzendaHours.Repository.RecordsRepository
{
    public class RecordsRepository : DatabaseConnection, IRecordsRepository
    {
        /// <summary>
        /// Select all records for the user as a List
        /// </summary>
        /// <param name="user"></param>
        /// <returns>A list of records for the current user</returns>
        public IList<Records> Get(string user)
        {
            IList<Records> record = null;

            using (IDbConnection conn = Connection)
            {
                conn.Open();
                if (!string.IsNullOrEmpty(user))
                {
                    record = conn.Query<Records>(@"SELECT r.*, p.Project, t.Task
                    FROM Records AS r
                    JOIN Projects AS p ON r.ProjectId = p.ProjectId
                    JOIN Tasks AS t ON r.TaskId = t.TaskId
                    WHERE EmployeeId=@UserName
                    ORDER BY EntryId", new { UserName = user }).ToList();
                }
                else
                {
                    record = conn.Query<Records>(@"SELECT r.*, p.Project, t.Task
                    FROM Records AS r
                    JOIN Projects AS p ON r.ProjectId = p.ProjectId
                    JOIN Tasks AS t ON r.TaskId = t.TaskId
                    ORDER BY EntryId").ToList();
                }
            }

            return record;
        }
        /// <summary>
        /// Select an individual record
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An individual record</returns>
        public Records GetById(int id)
        {
            Records record = null;

            using (IDbConnection conn = Connection)
            {
                conn.Open();
                record = conn.Get<Records>(id);
            }

            return record;
        }
        /// <summary>
        /// Add a new record
        /// </summary>
        /// <param name="record">Record object</param>
        /// <returns>Whether or not rows have been changed</returns>
        public bool Add(Records record)
        {
            int rowsAffected = 0;
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                rowsAffected = conn.Execute(@"INSERT INTO Records (EmployeeId, TaskId, CaseNo, ProjectId, Hours, WikiLink, Notes, RecordDate)
                                VALUES (@EmployeeId, @TaskId, @CaseNo, @ProjectId, @Hours, @WikiLink, @Notes, @RecordDate)",
                                 new
                                 {
                                     // TODO: Pass current user to Add
                                     EmployeeId = record.EmployeeId,
                                     TaskId = record.TaskId,
                                     CaseNo = record.CaseNo,
                                     ProjectId = record.ProjectId,
                                     Hours = record.Hours,
                                     WikiLink = record.WikiLink,
                                     Notes = record.Notes,
                                     RecordDate = record.RecordDate
                                 });
            }

            if (rowsAffected > 0)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Update a record in the database
        /// </summary>
        /// <param name="record">Record object</param>
        /// <returns>Whether or not rows were updated</returns>
        public bool Update(Records record)
        {
            bool rowsAffected = false;
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                rowsAffected = conn.Update(record);
            }

            return rowsAffected;
        }
        /// <summary>
        /// Delete a record from the database
        /// </summary>
        /// <param name="record">Record object</param>
        /// <returns>Whether or not rows were deleted</returns>
        public bool Delete(Records record)
        {
            bool rowsAffected = false;
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                rowsAffected = conn.Delete(record);
            }

            return rowsAffected;
        }
    }
}
