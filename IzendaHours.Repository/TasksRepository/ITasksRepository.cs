﻿using System.Collections.Generic;
using IzendaHours.Framework.Models;

namespace IzendaHours.Repository.TasksRepository
{
    internal interface ITasksRepository
    {
        IList<Tasks> GetTasks();
    }
}
