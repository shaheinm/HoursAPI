﻿using System.Collections.Generic;
using IzendaHours.Framework.Models;
using Dapper;
using System.Data;
using IzendaHours.Framework.DatabaseConnection;

namespace IzendaHours.Repository.TasksRepository
{
    public class TasksRepository : DatabaseConnection, ITasksRepository
    {
        /// <summary>
        /// Query database for Tasks List
        /// </summary>
        /// <returns></returns>
        public IList<Tasks> GetTasks()
        {
            IList<Tasks> task = null;

            using (IDbConnection conn = Connection)
            {
                conn.Open();
                task = conn.Query<Tasks>("SELECT * FROM Tasks ORDER BY TaskId").AsList();
            }

            return task;
        }
    }
}
