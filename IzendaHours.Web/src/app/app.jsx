import React from 'react';
import { render } from 'react-dom';
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Home from './home/home';
import Timesheet from './timesheet/timesheet';
import Projects from './projects/projects';
import Colors from 'material-ui/lib/styles/colors';
import ThemeManager from 'material-ui/lib/styles/theme-manager';
import IzAppBar from './shared/IzAppBar';
import IzPopover from './shared/IzPopover';
import IzTheme from './shared/IzTheme';
import CircularProgress from 'material-ui/lib/circular-progress';


const styles = {
  container: {
    textAlign: 'center',
    paddingTop: 200,
  },
};

const Main = React.createClass({
  getInitialState: function () {
    return { loading: true };
  },

  componentDidMount: function () {
    this.setState({ loading: false });
  },

  childContextTypes : {
    muiTheme: React.PropTypes.object,
  },

  getChildContext: function () {
    return {
      muiTheme: ThemeManager.getMuiTheme(IzTheme),
    };
  },

  render: function () {
        return (
          <div>
            <IzAppBar>
              <div>
                {this.props.children || <CircularProgress size={1.5} style={{paddingTop: '20%', paddingLeft: '50%'}}/>}
              </div>
            </IzAppBar>
            <IzPopover />
          </div>
        );
  },
})

//Needed for onTouchTap
//Can go away when react 1.0 release
//Check this repo:
//https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

// Render the main app react component into the app div.
// For more details see: https://facebook.github.io/react/docs/top-level-api.html#react.render
render((
  <Router history={browserHistory}>
    <Route path="/" component={Main}>
      <IndexRoute component={Home}/>
      <Route path="timesheet" component={Timesheet} />
      <Route path="projects" component={Projects} />
    </Route>
  </Router>),
  document.getElementById('app'));
