import React from 'react';
import Card from 'material-ui/lib/card/card';
import CardHeader from 'material-ui/lib/card/card-header';
import CardText from 'material-ui/lib/card/card-text';
import Divider from 'material-ui/lib/divider';
import $ from 'jquery';

let Config = require('Config');

let apiCallback = Config.serverUrl + 'record';

const ItemDetailList = React.createClass({
    render: function() {
        let emptyResults;
        let itemTable = this.props.data.map(function (itemarray) {

            return (
              <div>
                   <table id="ReactTable" style={{width: '100%', zIndex: '50000'}}>
                    <tbody>
                        <tr height="35" >
                        <td className="tableText" width="30%" align="center" >
                            {itemarray.project}
                        </td>
                        <td className="tableText" width="50%" align="center" >
                            {itemarray.notes}
                        </td>
                        <td className="tableText" width="20%" align="right">
                            {itemarray.hours}
                        </td>
                        </tr>
                    </tbody>
                   </table>
                   <Divider />
              </div>
          );
        });

        if (this.props.data.length === 0) {
            emptyResults = (<div className="noRecords">Nothing recorded today...</div>)
        };

        return (
          <div >
              {itemTable}
              {emptyResults}
          </div>
            );
    },
});

const ItemArray = React.createClass({
    render: function() {
        return (
          <div>
              {this.props.children}
          </div>
      );
    },
});

const ItemContainer = React.createClass({

    loadItemsFromServer: function(){
      $.ajax({
        url: apiCallback,
        dataType: 'json',
        cache: false,
        success: function(data) {
          this.setState({data: data});
        }.bind(this),
        error: function(xhr, status, err) {
          console.error(apiCallback, status, err.toString());
        }.bind(this),
      });
    },

    getInitialState: function(){
        return{data: []};
    },

    componentDidMount: function () {
        this.loadItemsFromServer();
        let intervalId = setInterval(this.loadTasksFromServer, 2000);
        this.setState({intervalId: intervalId});
    },

    componentWillUnmount: function () {
        clearInterval(this.state.intervalId);
    },

    render: function(){
        return(
          <div style={{
              display: 'flex',
              position:  'relative',
              left: '25%',
              top: '90px',
            }}>
          <Card style={{display: 'flex', overflowY: 'scroll', width: '60%', height: 300}} zDepth={3}>
            <CardHeader style={{paddingTop: '25px'}}>
              <span className="titleTest">Todays Hours</span>
            </CardHeader>
            <Divider />
            <CardText id="HoursCard" style={{ textAlign: 'center'}}>
             <ItemDetailList data={this.state.data } />
            </CardText>
          </Card>
        </div>
        );
},
});

export default ItemContainer;
