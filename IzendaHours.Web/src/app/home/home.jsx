import React from 'react';
import CircularProgress from 'material-ui/lib/circular-progress';
import ItemContainer from './HoursToday';
import ReportParts from './reports'

const styles = {
  container: {
    textAlign: 'center',
  },
};

const Home = React.createClass({
  getInitialState: function () {
    return { loading: true };
  },

  componentDidMount: function () {
    this.setState({ loading: false });
  },

  render: function () {
    if (this.state.loading) {
        return (
          <div>
            <CircularProgress />
          </div>
        );
      }
      else {
        return (
          <div>
            <ItemContainer />
            <ReportParts />
          </div>
        );
      }
  },
});

export default Home;
