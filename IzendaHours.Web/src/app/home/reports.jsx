import React from 'react';
import Paper from 'material-ui/lib/paper';

const style = {
  position: 'relative',
  height: 400,
  margin: 20,
  display: 'inline-flex',
};

const ReportParts = () => (
  <div style={{ marginTop: 120, marginLeft: '25%', width: '60%'}}>
    <Paper style={style} zDepth={3}><h2>Report Parts Go Here</h2></Paper>
    <Paper style={style} zDepth={3}><h2>More Report Parts Here</h2></Paper>
  </div>
);

export default ReportParts;
