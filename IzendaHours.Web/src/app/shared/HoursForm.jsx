import React from 'react';
import TextField from 'material-ui/lib/text-field';
import RaisedButton from 'material-ui/lib/raised-button';
import IzCustomerSelect from './IzCustomerSelect';
import IzTaskSelect from './IzTaskSelect';
import IzDatePicker from './IzDatePicker';
import $ from 'jquery';

let Config = require('Config');

const styles = {
  popover: {
    padding: 20,
    width: 325,
  },
};

const HoursForm = React.createClass({
  getInitialState: function() {
    return { hours: null, caseNo: null, notes: null, wikiLink: null }
  },

  handleCaseChange: function(e) {
    this.setState({caseNo: e.target.value});
  },
  handleNotesChange: function(e) {
    this.setState({notes: e.target.value});
  },

  handleHoursChange: function(e) {
    this.setState({hours: e.target.value});
  },
  handleWikiChange: function(e) {
    this.setState({wikiLink: e.target.value});
  },

  submit: function(e) {
    let self = this;

    e.preventDefault()

    const data = {
      employeeId: 'Shahein',
      taskId: self.refs.taskId.state.value,
      caseNo: self.state.caseNo,
      projectId: self.refs.projectId.state.value,
      hours: self.state.hours,
      wikiLink: self.state.wikiLink,
      notes: self.state.notes,
      recordDate: self.refs.recordDate.state.controlledDate.toJSON(),
    }

    $.ajax({
      url: Config.serverUrl + 'record',
      dataType: 'json',
      type: 'POST',
      data: data,
      success: function(data) {
        self.setState({data: data});
      }.bind(self),
      error: function(xhr, status, err) {
        console.error(Config.serverUrl + 'task', status, err.toString());
      }.bind(self),
    });

    console.log(e);

    self.setState({employeeId: '', taskId: '', caseNo: '', projectId: '', hours: '', wikiLink: '', notes: '', recordDate: ''});

  },

  render () {
    return(
      <form style={styles.popover} onSubmit={this.submit}>
        <h2>Create a New Record</h2>
        <IzDatePicker ref="recordDate" />
        <IzCustomerSelect ref="projectId" /><br/>
        <IzTaskSelect ref="taskId" />
        <TextField
            hintText="Is there a case number?"
            floatingLabelText="Case Number"
            value={this.state.caseNo}
            onChange={this.handleCaseChange}
        /><br />
        <TextField
          hintText="How long did you spend on it?"
          floatingLabelText="Hours"
          value={this.state.hours}
          onChange={this.handleHoursChange}
        /><br />
        <TextField
          hintText="Did you write a Wiki article?"
          floatingLabelText="Wiki"
          value={this.state.wikiLink}
          onChange={this.handleWikiChange}
        /><br />
        <TextField
          hintText="What did you do?"
          floatingLabelText="Notes"
          value={this.state.notes}
          onChange={this.handleNotesChange}
          multiLine={true}
          rows={2}
         /><br /><br />
       <RaisedButton primary={true} label="Submit" type="submit" value="Post"/>
      </form>
    );
  },
});

export default HoursForm;
