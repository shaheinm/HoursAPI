import React from 'react';
import AppBar from 'material-ui/lib/app-bar';
import IconButton from 'material-ui/lib/icon-button';
import IconMenu from 'material-ui/lib/menus/icon-menu';
import NavigationMenu from 'material-ui/lib/svg-icons/navigation/menu';
import MoreVertIcon from 'material-ui/lib/svg-icons/navigation/more-vert';
import MenuItem from 'material-ui/lib/menus/menu-item';
import LeftNav from 'material-ui/lib/left-nav';
import Divider from 'material-ui/lib/divider';
import { Link } from 'react-router';

export default class IzAppBar extends React.Component {

  constructor(props) {
    super(props);
    let leftNavOpen = function () {
      if (window.innerWidth < 700) {
        return false;
      }
      else {
        return true;
      }
    };
    this.state = {open: leftNavOpen()};
  }

  handleToggle = () => this.setState({open: !this.state.open});

  render() {
    return (
      <div>
        <AppBar
          title="Izenda Hours"
          iconElementLeft={<IconButton onTouchTap={this.handleToggle}><NavigationMenu /></IconButton>}
          iconElementRight={
            <IconMenu
              iconButtonElement={
                <IconButton><MoreVertIcon /></IconButton>
              }
              targetOrigin={{horizontal: 'right', vertical: 'top'}}
              anchorOrigin={{horizontal: 'right', vertical: 'top'}}
            >
              <MenuItem primaryText="Hello Shahein!" />
              <Divider />
              <MenuItem href="/" primaryText="Refresh" />
              <MenuItem href="/account" primaryText="My Account" />
              <MenuItem href="/signout" primaryText="Sign out" />
            </IconMenu>
          }
          style={{ position: 'fixed'}}
        >
        <LeftNav open={this.state.open} className="HoursLeftNav" style={{ position: 'fixed', top: '64px'}}>
          <MenuItem><Link to="/" style={{ textDecoration: 'none', color: 'inherit' }}>Home</Link></MenuItem>
          <MenuItem><Link to="/timesheet" style={{ textDecoration: 'none', color: 'inherit' }}>Timesheet</Link></MenuItem>
          <MenuItem><Link to="/projects" style={{ textDecoration: 'none', color: 'inherit' }}>Projects</Link></MenuItem>
          <MenuItem href="/reporting" style={{ textDecoration: 'none', color: 'inherit' }}>Reporting</MenuItem>
          <Divider />
          <MenuItem href="https://fogbugz.izenda.us"><span className="secondLeftMenu">Fogbugz</span></MenuItem>
          <MenuItem href="https://github.com/izenda/adhoc"><span className="secondLeftMenu">GitHub</span></MenuItem>
          <MenuItem href="http://wiki.izenda.us"><span className="secondLeftMenu">Wiki</span></MenuItem>
          <MenuItem href="http://confidential.izenda.us"><span className="secondLeftMenu">Confidential</span></MenuItem>
          <Divider />
          <MenuItem><span className="secondLeftMenu">Demo2/ci</span></MenuItem>
          <MenuItem><span className="secondLeftMenu">Demo2/newir</span></MenuItem>
          <MenuItem><span className="secondLeftMenu">Dev7/test</span></MenuItem>
          <MenuItem><span className="secondLeftMenu">Dev6/Synergy</span></MenuItem>
          <MenuItem><span className="secondLeftMenu">Dev7/anothertest</span></MenuItem>
          <MenuItem><span className="secondLeftMenu">Dev8/oneheretoo</span></MenuItem>
          <MenuItem><span className="secondLeftMenu">Izenda/demo</span></MenuItem>
        </LeftNav>
      </AppBar>
      {this.props.children}
      </div>
    );
  }
}
