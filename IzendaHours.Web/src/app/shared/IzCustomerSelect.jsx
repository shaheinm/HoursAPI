import React from 'react';
import SelectField from 'material-ui/lib/select-field';
import MenuItem from 'material-ui/lib/menus/menu-item';
import $ from 'jquery';

let Config = require('Config');
let apiCallback = Config.serverUrl + 'project';

const IzCustomerSelect = React.createClass({

  loadCustomersFromServer: function(){
    $.ajax({
      url: apiCallback,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(apiCallback, status, err.toString());
      }.bind(this),
    });
  },

  getInitialState: function(){
      return{data: []};
  },

  componentDidMount: function () {
      this.loadCustomersFromServer();
      setInterval(this.loadItemsFromServer, 2000);
  },

  handleChange: function(e, index, value) { this.setState({value}); },

  render: function () {
      let customers = this.state.data.map(function(custarray) {
        return (
          <MenuItem value={custarray.projectId} primaryText={custarray.project} />
        );
      });
    return (
      <SelectField
        maxHeight={300}
        floatingLabelText="Customer"
        value={this.state.value}
        data={this.state.data}
        onChange={this.handleChange}
        style={{overflow: 'hidden'}}>
        {customers}
      </SelectField>
      );
    },
});

export default IzCustomerSelect;
