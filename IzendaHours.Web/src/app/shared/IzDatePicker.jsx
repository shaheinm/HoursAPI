import React from 'react';
import DatePicker from 'material-ui/lib/date-picker/date-picker';

export default class IzDatePicker extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      controlledDate: new Date(),
    };
  }

  _handleChange = (event, date) => {
    this.setState({
      controlledDate: date,
    });
  };

  render() {
    return (
      <DatePicker
        hintText="Controlled Date Input"
        value={this.state.controlledDate}
        onChange={this._handleChange}
        container="inline"
      />
    );
  }
}
