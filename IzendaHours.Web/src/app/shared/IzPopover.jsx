import React from 'react';
import Popover from 'material-ui/lib/popover/popover';
import FloatingActionButton from 'material-ui/lib/floating-action-button';
import ContentAdd from 'material-ui/lib/svg-icons/content/add';
import HoursForm from './HoursForm';

export default class IzPopover extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      value: null,
    };
  }

  handleTouchTap = (event) => {
    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  handleChange = (event, index, value) => this.setState({value});

  render() {
    return (
      <div style={{
          position: 'fixed',
          bottom: '10px',
          right: '10px',
        }}>
        <FloatingActionButton
          onTouchTap={this.handleTouchTap}
          label="Click me" >
          <ContentAdd />
        </FloatingActionButton>
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'right', vertical: 'top'}}
          targetOrigin={{horizontal: 'right', vertical: 'bottom'}}
          onRequestClose={this.handleRequestClose}
        >
          <HoursForm />
        </Popover>
      </div>
    );
  }
}
