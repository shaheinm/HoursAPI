import React from 'react';
import SelectField from 'material-ui/lib/select-field';
import MenuItem from 'material-ui/lib/menus/menu-item';
import $ from 'jquery';

let Config = require('Config');

let apiCallback = Config.serverUrl + 'task';

const IzTaskSelect = React.createClass({

  loadTasksFromServer: function(){
    $.ajax({
      url: apiCallback,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(apiCallback, status, err.toString());
      }.bind(this),
    });
  },

  getInitialState: function(){
      return{data: []};
  },

  componentDidMount: function () {
      this.loadTasksFromServer();
      let intervalId = setInterval(this.loadTasksFromServer, 2000);
      this.setState({intervalId: intervalId});
  },

  handleChange: function(e, index, value) { this.setState({value}); },

  componentWillUnmount: function () {
    clearInterval(this.state.intervalId);
  },

  render: function () {
      let tasks = this.state.data.map(function(taskarray) {
        return (
          <MenuItem value={taskarray.taskId} primaryText={taskarray.task} />
        );
      });
    return (
      <SelectField maxHeight={300} floatingLabelText="Task" value={this.state.value} data={this.state.data} onChange={this.handleChange}>
        {tasks}
      </SelectField>
      );
    },
});

export default IzTaskSelect;
