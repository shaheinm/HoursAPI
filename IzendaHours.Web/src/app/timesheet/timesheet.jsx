import React from 'react';
import CircularProgress from 'material-ui/lib/circular-progress';
import Timetable from './timetable';

const Timesheet = React.createClass({
  getInitialState: function () {
    return { loading: true };
  },

  componentDidMount: function () {
    this.setState({ loading: false });
  },

  render: function () {
    if (this.state.loading) {
        return (
          <div>
            <CircularProgress />
          </div>
        );
      }
      else {
        return (
          <div>
            <Timetable />
          </div>
        );
      }
  },
});

export default Timesheet;
