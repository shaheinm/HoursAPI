#Izenda Hours App
Entirely new application!  You will need the latest node/npm installed on your machine to get started.  I would also recommend a text editor with JSX syntax highlighting.

##Making Changes
Please do not make changes directly to the master branch.  If you have changes to add, you can fork the repository and create a pull request (preferred), or you can create another branch and go over the changes with me.

##Getting Started
1. Clone the repository.
2. In the IzendaHours.Web directory, run ```npm install```
3. Open the solution file in Visual Studio (built and tested only in VS 2015).
4. Restore nuget packages to the IzendaHours.API project (you may need to rebuild/restart VS).
5. Set the connection string in the Web.config for the IzendaHours.API project to your Hours database (you can download a test database at https://github.com/smoussavi/IzendaHours - the db is in the App_Data.zip file).
6. Run the IzendaHours.API project in VS.
7. Note the port that the API is running on (default is 40247) and confirm it is the same in the IzendaHours.Web project, webpack-dev-server.config.js file line 69.
8. Back in the console, in the IzendaHours.Web directory, run ```npm run-script start```
9. Navigate to http://localhost:8085 

##Moving Around the Folder Structure
There are two projects here.  The API requires ASP.NET, and thus needs to be run in dev from Visual Studio.  The Web project uses Webpack, and is run from the command line scripts mentioned above.  

###The API
The Modules folder in the API contains the routes for each piece of the application (it could be seen as the controller).  Inside of the Models folder, you will see PetaPoco.cs (please don't change anything here - it's a tiny ORM whose only dependency is that C# file) and HoursContext.cs, which contains the POCOs and database connection setup for the application.

Please refer to the [NancyFx](https://github.com/NancyFx/Nancy) documentation if you want to learn more about what you can do within the framework.

###The Web Project
All of the React files are located inside the src folder.  The project is configured and built using Webpack.  The use of Babel allows the use of ES6+ syntax, so please refer to the ES6 and Babel documentation.  Material UI is used and extended to create the front-end layout.  It is helpful to have some knowledge of the following to work within the app:

- [React](https://facebook.github.io/react)
- [Webpack](https://webpack.github.io)
- [Babel](https://babeljs.io)
- [Material UI](http://www.material-ui.com)
- [React Router](https://github.com/reactjs/react-router)

##Roadmap
There are still several tasks that need to be completed to get this application into production, including:

- Authentication - Main hurdle is passing tokens securely from the JavaScript SPA to the C# server.
- Timesheet and Projects pages
- Integration of Izenda
